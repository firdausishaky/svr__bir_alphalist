
return module.exports = {

	db_ : {
		local : { // in use on local 
		  /*host     : '10.129.1.16',
		  user     : 'root',
		  socketPath : '/var/lib/mysql/mysql.sock'*/
		  host 		: '192.168.1.12',
		  user 		: 'root',
		  password 	: 'asdf1234*',
		  database 	: "payroll",
		  waitForConnections: true,
		  connectionLimit: 10,
		  queueLimit: 0
		},
		uat : {	
		  /*host     : '10.129.1.16',
		  user     : 'root',
		  socketPath : '/var/lib/mysql/mysql.sock'*/
		  host 		: '192.168.1.12',
		  user 		: 'root',
		  password 	: 'asdf1234*',
		  database 	: "payroll",
		  waitForConnections: true,
		  connectionLimit: 10,
		  queueLimit: 0
		},
		uat_bak : {
			host     : '192.168.1.12',
			user     : 'root',
			password 	: '',
			database 	: "payroll",
			waitForConnections: true,
			connectionLimit: 10,
			queueLimit: 0,
			socketPath : '/var/lib/mysql/mysql.sock'
		}

	},
	ips : {
		/*uat 	: "hrmsapi2uat.leekie.com:8080",*/
		uat : "192.168.1.12:8080",
		local 	: "192.168.1.12:8080", // chang your local ip
		my_pc 	: "192.168.1.12:8080"
	}

}