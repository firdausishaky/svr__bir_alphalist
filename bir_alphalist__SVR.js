var express = require("express");
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express()
var request = require("request");
//var mysql      = require('mysql');
var mysql      = require('mysql2'); // upgrade
var async = require("async");
const Joi = require('@hapi/joi');

const routes_cfg = require('./libs/route_config');
const compute = require('./libs/all_rates');

var libs_res = require('./libs/database_libs');
var Call_procedure = libs_res.Call_procedure;
var result_sql = libs_res.result_sql;
var result_sql2 = libs_res.result_sql2;


let secret = null;

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const db_config = routes_cfg.db_.uat;

const set_ip = routes_cfg.ips.uat;


// let secret = null;

// app.use(cors())

// // parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))

// // parse application/json
// app.use(bodyParser.json())


// const db_ = {
// 	local : {
// 	  host 		: '127.0.0.1',
// 	  user 		: 'root',
// 	  password 	: 'asdf1234*',
// 	  database 	: "payroll",
// 	  waitForConnections: true,
// 	  connectionLimit: 10,
// 	  queueLimit: 0
// 	},
// 	uat : {
// 	  host     : '10.129.1.16',
// 	  user     : 'isilabs',
// 	  //socketPath : '/var/lib/mysql/mysql.sock'
// 	}
// };

// const db_config = db_.local;

// const ips = {
// 	uat 	: "hrmsapi2uat.leekie.com:8080",
// 	local 	: "127.0.0.1:8080", // chang your local ip
// 	my_pc 	: "120.0.01:8080"
// };

// const set_ip = ips.local;

function handleDisconnect() {
	connection = mysql.createConnection(db_config);


	connection.connect(function(err) {
		if(err) {

			setTimeout(handleDisconnect, 2000);
		}
		else{
			console.log('connected as id ' + connection.threadId);
		}
	});

	connection.on('error', function(err) {

		if(err.code === 'PROTOCOL_CONNECTION_LOST') {
			handleDisconnect();
		} else {
			return console.log(err,"DB");
		}
	});
}

handleDisconnect();

  const msg_validate = { header : {"message":"validation error", status:500},data : []};
const msg_empty = { header : {"message":"Data is Empty", status:200}, data : []};
var msg_success = { header : { message : "Request is Success", status:200 }, data : null};


	// function result_sql(dt,res) {
 //  		try{
 //  			if(dt[0].result){
 //  				var split = dt[0].result.split(",");
	//   			if(split[1] == 200){
	//   				return res.status(200).json(msg_success);
	//   			}else{
	//   				return res.status(200).json(msg_empty);
	//   			}
	//   		}else{
	//   			msg_success.data = dt;
	//   			return res.status(200).json(msg_success);
	//   		}
	//   	}catch(e){
	//   		/*if(dt.length > 0){
	//   			msg_success.data = dt;
	//   			return res.status(200).json(msg_success);
	//   		}*/
	//   		return res.status(404).json(msg_empty);
	//   		console.log(e,"data");
	//   	}
	// }

	// function Call_procedure(name,params, cbs){
	// 	async.waterfall([
	// 		(cb)=>{

	// 			var params_array = Object.values(params);

	// 			connection.execute("call GET_PARAM(?)",[name],(e,r,f)=>{
	// 				if(e){ return console.log(e,"Call_procedure::001");}
	// 				else{

	// 					var validations = {};
	// 					var count_params = [];

	// 					for(var i=0; i < r[0].length; i++){

	// 						var keys = r[0][i].PARAMETER_NAME;
	// 						var keys = keys.substring(1,keys.length);
	// 						if(keys == 'active'){ params_array.push(1); params[keys] = 1;}

	// 							count_params.push("?");

	// 							if(r[0][i].DATA_TYPE == 'varchar' || r[0][i].DATA_TYPE == 'date'){

	// 								// OLD MYSQL with driver
	// 								// params[keys] = "'"+params[keys]+"'";
	// 								params[keys] = ""+params[keys]+"";
	// 							}
	// 							if(!params[keys]){ validations[keys] = 0;	 }
	// 							else{ validations[keys] = params[keys]; }

	// 					}

	// 					cb(false, Object.values(validations),count_params.toString(), name);
	// 				}
	// 			});
	// 		},(valid, params_tag, procedure, cb)=>{
	// 			cb(false, valid, params_tag, procedure);
	// 		}
	// 	],(err, valid, params_tag, procedure)=>{
	// 		cbs(false, { input : valid, params_tag: params_tag, procedure : procedure});
	// 	})
	// }

	// function result_sql2(dt3,respon) {
 //  		try{
 //  			if(dt3[0].result){
 //  				var split = dt3[0].result.split(",");
	//   			if(split[1] == 200){
	//   				return respon.status(200).json(msg_success);
	//   			}else{
	//   				return respon.status(200).json(msg_empty);
	//   			}
	//   		}else{
	//   			msg_success.data = dt3;
	//   			return respon.status(200).json(msg_success);
	//   		}
	//   	}catch(e){

	//   		if(dt3.length > 0){
	//   			msg_success.data = dt3;
	//   			return respon.status(200).json(msg_success);
	//   		}
	//   		return respon.status(404).json(dt3);
	//   	}
	// }

	// ================================================ CUTOFF PER PAYROLL =================================================

	app.get("/bir_alphalist/get",(req,res)=>{

		async.waterfall([
			(cb)=>{
				connection.execute("call cutoff_periode_date__GET()", (e,r)=>{
					if(e){ return res.status(500).json(e);}
					else{

						if(r[0].length > 0){ cb(false,/*r[0]*/[2019]);  }
						else{ cb(r[0], false);  }

					}
				})
			},
			(dt_cutoff_periode_date, cb)=>{
				connection.execute("call department__GET()", (e,r)=>{
					if(e){ return res.status(500).json(e);}
					else{
						var obj = {
							department : r[0],
							cutoff_periode : dt_cutoff_periode_date
						};
						cb(false, obj);
					}
				})
			}
		],(e,r)=>{
			if(e){
				msg_empty.header.message = e;
				return res.status(msg_empty.header.status).json(msg_empty);
			}else{
				msg_success.data  = r;
				return res.status(msg_success.header.status).json(msg_success);
			}
			//result_sql(r,res);
		});
	});

	/*
		SEARCH DATA WITH PARAM DEPARTMENT, CUTOFF_DATE, NAME
	*/
	app.post("/bir_alphalist/search_by_name",(req,res)=>{

		async.waterfall([
			(cb)=>{
				Joi.validate(req.body,{
					name 	: Joi.string().required()

				},function(e,i){
					if(e){ cb(msg_validate, false); }
					else{ cb(false, i); }
				});
			},
			(input, cb)=>{
				Call_procedure('search_employee_by_name',input,(e,d)=>{
					if(e){ return cb(e, false); }
					connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
						if(e){ return cb(e, false); }
						else{ cb(false, r); }
					})
				});
			}
		],(e,r)=>{
			if(e){
				msg_empty.header.message = e;
				return res.status(msg_empty.header.status).json(msg_empty);
			}else{
				return res.status(msg_success.header.status).json(msg_success);
			}
			// if(e){ r[0] = e; }
			// result_sql(r,res);
		});
	});

	app.post("/bir_alphalist/search2",(req,res2)=>{

		async.waterfall([
			(cb)=>{
				Joi.validate(req.body,{
					employee_id 	: Joi.string(),
					department 		: Joi.number(),
					start_date 		: Joi.string().required(),
					end_date 		: Joi.string().required()

				},function(e,i){
					if(e){ cb(msg_validate, false); }
					else{ cb(false, i); }
				});
			},
			(input, cb)=>{
				return console.log(input,"BIR====================")
			}
		],(err,r)=>{
			if(err){ r[0] = err; result_sql(r[0],res2);}
			else{
				result_sql2(r,res2);
			}

		});
	});

	app.post("/bir_alphalist/insert",(req,res)=>{

		async.waterfall([
			(cb)=>{
				Joi.validate(req.body,{
					cutoff_by 		: Joi.string().required(),
					department_id 	: Joi.number().required(),
					start_date 		: Joi.string().required(),
					end_date 		: Joi.string().required(),
					account 		: Joi.array(),
					subtotal 		: Joi.object()
				},function(e,i){
					if(e){ res.status(500).json(e) }
					else{ cb(false, i); }
				});
			},
			(input, cb)=>{
				var dates 	= new Date(input.start_date);
				var year 	= dates.getFullYear();
				var month 	= dates.getMonth();
				month++;

				input.year = year;
				input.month = month;
				Call_procedure('last_pay_sheet__INSERT',input,(e,d)=>{

					if(e){ return cb(e, false); }

					connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
						if(e){ cb(e, false); }
						else{ cb(false, r); }
					})
				});
			}
		],(e,r)=>{
			//return res.status(200).json(e);
			if(e){ r[0] = e; }
			result_sql([r[0]],res);
		});
	});






	app.post("/get_total_taxable_year",(req,res2)=>{

		async.waterfall([
			(cb1)=>{

					connection.execute("call cutoff_data_by_month()", (e,r)=>{
						if(e){ return console.log(11);/*cb(e, false);*/ }
						else{

							var tmp_employee_id = [];
							if(r[0].length > 0){

								for(var i in r[0]){

									var tmp_json 		= JSON.parse(r[0][i].cutoff_per_payroll);
									r[0][i].json_data 	= tmp_json;
									r[0][i].json_data_price = tmp_json;
									r[0][i].employee_id = tmp_json.employee_id;
								}

								return cb1(false, r);
							}else{ return console.log(12);/*cb(msg_empty, false);*/ }
						}
					})

			},(cutoff, cb)=>{ // compute data object
				//return console.log(13);
				//const compute = require('./libs/all_rates');

				var tmp_computes = [ ];
				var cutoff_build = [];
				var months = [];
				cutoff = cutoff[0];

				for(var i=0; i < cutoff.length; i++){

					try{

							// HOLIDAY =========================
							var rh2 = cutoff[i].json_data_price.holiday_work.rh;
							if(rh2 > 0){
								basic = Number(cutoff[i].daily_salary);
								ecola = Number(cutoff[i].daily_ecola);
								rh2  = compute.count_rh(basic, ecola, rh2);
							}
							cutoff[i].json_data_price.holiday_work.rh = rh2;

							var sh2 = cutoff[i].json_data_price.holiday_work.sh;
							if(sh2 > 0){
								basic = Number(cutoff[i].daily_salary);
								ecola = Number(cutoff[i].daily_ecola);
								sh2  = compute.count_sh(basic, ecola, sh2);
							}
							cutoff[i].json_data_price.holiday_work.sh 		= sh2;
							cutoff[i].json_data_price.holiday_work.total 	=  rh2+sh2;
							// ====================================


							// OVERTIME ==============
							const got = cutoff[i].json_data_price.overtime_of_hours.got;
							const basic_hour 		= Number(cutoff[i].hourly_salary);
							const ecola_hour 		= Number(cutoff[i].hourly_ecola);
							if(got > 0){

								cutoff[i].json_data_price.overtime_of_hours.got  = compute.count_got(basic_hour, ecola_hour, got);
							}

							const rot = cutoff[i].json_data_price.overtime_of_hours.rot;
							if(rot > 0){
								cutoff[i].json_data_price.overtime_of_hours.rot  = compute.count_rot(basic_hour, ecola_hour,rot);
							}

							const rhot = cutoff[i].json_data_price.overtime_of_hours.rhot;
							if(rhot > 0){
								cutoff[i].json_data_price.overtime_of_hours.rhot  = compute.count_rhot(basic_hour, ecola_hour,rhot);
							}

							const shot = cutoff[i].json_data_price.overtime_of_hours.shot;
							if(shot > 0){
								cutoff[i].json_data_price.overtime_of_hours.shot  = compute.count_shot(basic_hour, ecola_hour,shot);
							}

							const rotdo_1st = cutoff[i].json_data_price.overtime_of_hours.rotdo_1st;
							if(rotdo_1st > 0){
								cutoff[i].json_data_price.overtime_of_hours.rotdo_1st  = compute.count_rotdo_1st(basic_hour, ecola_hour,rotdo_1st);
							}

							const rotdo_excess = cutoff[i].json_data_price.overtime_of_hours.rotdo_excess;
							if(rotdo_excess > 0){
								cutoff[i].json_data_price.overtime_of_hours.rotdo_excess  = compute.count_rotdo_excess(basic_hour, ecola_hour,rotdo_excess);
							}

							const rhrdot_1st = cutoff[i].json_data_price.overtime_of_hours.rhrdot_1st;
							if(rhrdot_1st > 0){
								cutoff[i].json_data_price.overtime_of_hours.rhrdot_1st  = compute.count_rhrdot_1st(basic_hour, ecola_hour,rhrdot_1st);
							}

							const rhrdot_excess = cutoff[i].json_data_price.overtime_of_hours.rhrdot_excess;
							if(rhrdot_excess > 0){
								cutoff[i].json_data_price.overtime_of_hours.rhrdot_excess  = compute.count_rhrdot_excess(basic_hour, ecola_hour,rhrdot_excess);
							}

							const shrdot_1st = cutoff[i].json_data_price.overtime_of_hours.shrdot_1st;
							if(shrdot_1st > 0){
								cutoff[i].json_data_price.overtime_of_hours.shrdot_1st  = compute.count_shrdot_1st(basic_hour, ecola_hour,shrdot_1st);
							}

							const shrdot_excess = cutoff[i].json_data_price.overtime_of_hours.shrdot_excess;
							if(shrdot_excess > 0){
								cutoff[i].json_data_price.overtime_of_hours.shrdot_excess  = compute.count_shrdot_excess(basic_hour, ecola_hour,shrdot_excess);
							}

							// total overtime
							cutoff[i].json_data_price.overtime_of_hours.total = (got+rot+rhot+shot+rotdo_1st+rotdo_excess+rhrdot_1st+rhrdot_excess+shrdot_1st+shrdot_excess);
							//return console.log(got,rot,rhot,shot,rdot_1st,rdot_excess,rhrdot_1st,rhrdot_excess,shrdot_1st,shrdot_excess,"============");

							const rnd = cutoff[i].json_data_price.night_differential.nd;
							if(rnd > 0){
								cutoff[i].json_data_price.night_differential.rnd  = compute.count_rnd(basic_hour, ecola_hour,rnd);
							}

							const rhnd = cutoff[i].json_data_price.night_differential.rhnd;
							if(rhnd > 0){
								cutoff[i].json_data_price.night_differential.rhnd  = compute.count_rhnd(basic_hour, ecola_hour,rhnd);
							}

							const shnd = cutoff[i].json_data_price.night_differential.shnd;
							if(shnd > 0){
								cutoff[i].json_data_price.night_differential.shnd  = compute.count_shnd(basic_hour, ecola_hour,shnd);
							}

							const rdnd = cutoff[i].json_data_price.night_differential.rdnd;
							if(rdnd > 0){
								cutoff[i].json_data_price.night_differential.rdnd  = compute.count_rdnd(basic_hour, ecola_hour,rdnd);
							}

							const rhrdnd = cutoff[i].json_data_price.night_differential.rhrdnd;
							if(rhrdnd > 0){
								cutoff[i].json_data_price.night_differential.rhrdnd  = compute.count_rhrdnd(basic_hour, ecola_hour,rhrdnd);
							}

							const shrdnd = cutoff[i].json_data_price.night_differential.shrdnd;
							if(shrdnd > 0){
								cutoff[i].json_data_price.night_differential.shrdnd  = compute.count_shrdnd(basic_hour, ecola_hour,shrdnd);
							}

							const ndot = cutoff[i].json_data_price.night_differential.ndot;
							if(ndot > 0){
								cutoff[i].json_data_price.night_differential.ndot  = compute.count_ndot(basic_hour, ecola_hour,ndot);
							}

							// total overtime nd
							cutoff[i].json_data_price.night_differential.total = (rnd+rhnd+shnd+rdnd+rhrdnd+shrdnd+ndot);

							const tardines = cutoff[i].json_data_price.tardiness;

							cutoff[i].json_data_price.tardiness = compute.count_tardiness(
								Number(cutoff[i].minutely_salary),
								Number(cutoff[i].minutely_ecola),
								tardines );


							const monthly_salary_rate = {
								basic : Number(cutoff[i].basic),
								ecola : Number(cutoff[i].ecola),
								other_allowance : Number(cutoff[i].other_allowance) || 0,
								get total(){
									var a = this.basic + this.ecola + this.other_allowance;
									return a;
								}
							};

							const daily_salary_rate = {
									basic : Number(cutoff[i].daily_salary),
									ecola : Number(cutoff[i].daily_ecola),
									other_allowance : Number(cutoff[i].other_allowance) || 0,
									get total(){
										var a = this.basic + this.ecola + this.other_allowance;
										return a;
									}
								};
							const hourly_salary_rate = {
									basic : Number(cutoff[i].hourly_salary),
									ecola : Number(cutoff[i].hourly_ecola),
									other_allowance : Number(cutoff[i].other_allowance) || 0,
									get total(){
										var a = this.basic + this.ecola + this.other_allowance;
										return a;
									}
							};
							const minutely_salary_rate = {
									basic : Number(cutoff[i].minutely_salary),
									ecola : Number(cutoff[i].minutely_ecola),
									other_allowance : Number(cutoff[i].other_allowance) || 0,
									get total(){
										var a = this.basic + this.ecola + this.other_allowance;
										return a;
									}
							};

							const percutoff_salary_rate = {
									basic : Number(cutoff[i].cutoff_salary),
									ecola : Number(cutoff[i].cutoff_ecola),
									other_allowance : Number(cutoff[i].other_allowance) || 0,
									get total(){
										var a = this.basic + this.ecola + this.other_allowance;
										return a;
									}
								};

							var tmp_compute = {
								monthly_salary_rate : monthly_salary_rate,
								daily_salary_rate : daily_salary_rate,
								hourly_salary_rate : hourly_salary_rate,
								minutely_salary_rate : minutely_salary_rate,
								percutoff_salary_rate : percutoff_salary_rate,

								total_absences : {

									//console.log(cutoff[i].json_data.absences_of_days,"================= "+i+" ===============")

										basic : monthly_salary_rate.basic,
										ecola : monthly_salary_rate.ecola,
										other_allowance : monthly_salary_rate.other_allowance,
										total : (monthly_salary_rate.basic * cutoff[i].json_data.absences_of_days.total) + (monthly_salary_rate.ecola * cutoff[i].json_data.absences_of_days.total)
											//return (this.basic * cutoff[i].json_data.absences_of_days.total) + (this.ecola * cutoff[i].json_data.absences_of_days.total);

								},
								total_leave_pay : {

										basic : monthly_salary_rate.basic,
										ecola : monthly_salary_rate.ecola,
										other_allowance : monthly_salary_rate.other_allowance,
										total : (monthly_salary_rate.basic * cutoff[i].json_data.absences_of_days.total) + (monthly_salary_rate.ecola * cutoff[i].json_data.absences_of_days.total)


								},
								total_holiday_pay : {

										basic : daily_salary_rate.basic,
										ecola : daily_salary_rate.ecola,
										other_allowance : 0,
										total : cutoff[i].json_data_price.holiday_work.total

								},
								total_overtime_pay : {

										basic : hourly_salary_rate.basic,
										ecola : hourly_salary_rate.ecola,
										other_allowance : 0,
										total : cutoff[i].json_data_price.overtime_of_hours.total

								},
								total_nd_pay : {

									basic : hourly_salary_rate.basic,
									ecola : hourly_salary_rate.ecola,
									other_allowance : 0,
									total : cutoff[i].json_data_price.night_differential.total

								},
								total_tardiness_pay : {

										basic : minutely_salary_rate.basic,
										ecola : minutely_salary_rate.ecola,
										other_allowance : 0,
										total : cutoff[i].json_data_price.tardiness

								},
								other_adjustment : {
									basic : null,
									ecola : null,
									other_allowance : 0,
									total : null
								},
								total_gross_salary_this_cutoff : {
									basic : (
										percutoff_salary_rate.basic +
										(
											(percutoff_salary_rate.basic * cutoff[i].json_data.absences_of_days.total)/*leave with pay*/ +
											cutoff[i].json_data_price.holiday_work.total
										))-
										(
											(percutoff_salary_rate.basic * cutoff[i].json_data.absences_of_days.total)+ cutoff[i].json_data_price.tardiness
										),
									ecola : (
										percutoff_salary_rate.ecola +
										(
											(percutoff_salary_rate.ecola * cutoff[i].json_data.absences_of_days.total) /*leave with pay*/ +
											cutoff[i].json_data_price.holiday_work.total
										))-
										(
											(percutoff_salary_rate.ecola * cutoff[i].json_data.absences_of_days.total) + cutoff[i].json_data_price.tardiness
										),
									other_allowance : 0,
									get total(){
										return (this.basic + this.ecola + this.other_allowance);
									}
								},
								gov : null,
								deduction : {
									sss : cutoff[i].sss,
									philhealth : cutoff[i].philhealth,
									hdmf : cutoff[i].hdmf,
									wtax : null,
									sss_loan : null,
									hdmf_loan : null,
									hmo : null,
									cash_advance : null,
									other : null,
									total : null
								},
								total_sss_phic_hdmf : (cutoff[i].sss+cutoff[i].philhealth+cutoff[i].hdmf)
							};

							var idx2 = cutoff_build.findIndex((str)=>{
								return str.employee_id == cutoff[i].employee_id;
							});

							if(idx2 == -1){
								months.push({'employee_id':cutoff[i].employee_id,month:[cutoff[i].month],year:cutoff[i].year});
								cutoff_build.push({
									employee_id : cutoff[i].employee_id,
									tin : cutoff[i].tin,
									tax_status : cutoff[i].tax_status,
									department_id : cutoff[i].department_id,
									total_monthly_salary : tmp_compute.monthly_salary_rate.total,
									total_daily_salary : tmp_compute.daily_salary_rate.total,
									total_hourly_salary : tmp_compute.hourly_salary_rate.total,
									total_minutely_salary : tmp_compute.minutely_salary_rate.total,
									total_percutoff_salary : tmp_compute.percutoff_salary_rate.total,
									total_absences : tmp_compute.total_absences.total,
									total_leave_pay : tmp_compute.total_leave_pay.total,
									total_holiday_pay : tmp_compute.total_holiday_pay.total,
									total_overtime_pay : tmp_compute.total_overtime_pay.total,
									total_nd_pay : tmp_compute.total_nd_pay.total,
									total_tardiness_pay : tmp_compute.total_tardiness_pay.total,
									other_adjustment : tmp_compute.other_adjustment.total,
									total_gross_salary_this_cutoff : tmp_compute.total_gross_salary_this_cutoff.total,
									deduction : tmp_compute.deduction,
									total_sss_phic_hdmf : tmp_compute.total_sss_phic_hdmf,
									total_ecola : tmp_compute.monthly_salary_rate.ecola

								})
							}else{
								cutoff_build[idx2].total_monthly_salary += tmp_compute.monthly_salary_rate.total;
								cutoff_build[idx2].total_daily_salary += tmp_compute.daily_salary_rate.total;
								cutoff_build[idx2].total_hourly_salary += tmp_compute.hourly_salary_rate.total;
								cutoff_build[idx2].total_minutely_salary += tmp_compute.minutely_salary_rate.total;
								cutoff_build[idx2].total_percutoff_salary += tmp_compute.percutoff_salary_rate.total;
								cutoff_build[idx2].total_absences += tmp_compute.total_absences.total;
								cutoff_build[idx2].total_leave_pay += tmp_compute.total_leave_pay.total;
								cutoff_build[idx2].total_holiday_pay += tmp_compute.total_holiday_pay.total;
								cutoff_build[idx2].total_overtime_pay += tmp_compute.total_overtime_pay.total;
								cutoff_build[idx2].total_nd_pay += tmp_compute.total_nd_pay.total;
								cutoff_build[idx2].total_tardiness_pay += tmp_compute.total_tardiness_pay.total;
								cutoff_build[idx2].other_adjustment += tmp_compute.other_adjustment.total;
								cutoff_build[idx2].total_gross_salary_this_cutoff += tmp_compute.total_gross_salary_this_cutoff.total;
								cutoff_build[idx2].deduction = tmp_compute.deduction;

								var idx3 = months.findIndex((str)=>{
									if(str.employee_id == cutoff[i].employee_id && str.year == cutoff[i].year){
										if(str.month.indexOf(cutoff[i].month) == -1){
											return true
										}
									}

								});
								if(idx3 != -1){
									months[idx3].month.push(cutoff[i].month);
									cutoff_build[idx2].total_sss_phic_hdmf += tmp_compute.total_sss_phic_hdmf;
									total_ecola += tmp_compute.monthly_salary_rate.ecola;
								}
								//console.log(cutoff_build[idx2],'===================================999999999999999999========================')
							}

							//cutoff[i].json_final = tmp_compute;

					}catch(ezz){
						console.log(ezz,"================== EEE ===================")
					}
					//delete cutoff[i].json_data_price;
				}
					cb(false, cutoff_build )

			},(cutoff, cb)=>{
				/*for(var i=0; i < cutoff.length; i++){
					cutoff[i].total_basic_ = (cutoff[i].total_monthly_salary + cutoff[i].total_holiday_pay + cutoff[i].total_overtime_pay + cutoff[i].total_leave_pay + cutoff[i].total_nd_pay) - (cutoff[i].total_absences + cutoff[i].total_tardiness_pay + cutoff[i].total_sss_phic_hdmf);
				}*/

				async.eachSeries(cutoff,(str,next)=>{
					async.setImmediate(function () {

						async.waterfall([
							(cb_1)=>{

								connection.query("select * from `cut_off_years` where `employee_id` = ? and `year` = ?",[str.employee_id,str.year],(e,r)=>{
									if(e){ return console.log(e,114);/*cb(e, false);*/ }
									else{

										var tmp_employee_id = [];
										if(r.length > 0){

											str.pay13th_other_benefit_below82000 = r[0][0].pay13th + r[0][0].performance_bonus + r[0][0].management_bonus + total_monthly_incentive;

											if(str.pay13th_other_benefit_below82000 > 82000){
												str.pay13th_other_benefit_excess82000 = str.pay13th_other_benefit_below82000 - 82000;
											}else{
												str.pay13th_other_benefit_excess82000 = 0;
											}

											//return cb1(false, r);
										}else{
											str.pay13th_other_benefit_below82000 	= 0;
											str.pay13th_other_benefit_excess82000 	= 0;
										}
										cb_1(false, str);
									}
								})

							},(str, cb_1)=>{

								connection.query("select * from `service_incentive_cutoff` where `employee_id` = ? and `year` = ?",[str.employee_id,str.year], (e,r)=>{
									if(e){ return console.log(11);/*cb(e, false);*/ }
									else{

										var tmp_employee_id = [];
										if(r.length > 0){

											var total_days_sil = r[0][0].vl + r[0][0].sl;
											if(total_days_sil > 10){
												str.sil_convert_excess = ((total_days_sil - 10) * r[0][0].daily_rate);
											}else{
												str.sil_convert_below = str.total_ecola + r[0][0].total_convertion;
											}
										}else{
											str.sil_convert_below 	= str.total_ecola;
											str.sil_convert_excess 	= 0;
										}
										cb_1(false, str);
									}
								})
							}
						],(err, str)=>{
							str = str;
							next(null);
						})
					});
				},(err)=>{
					var tmp = []
					for(var i=0; i < cutoff.length; i++){

						var total_non_tax = cutoff[i].pay13th_other_benefit_below82000 + cutoff[i].total_sss_phic_hdmf + cutoff[i].sil_convert_below;
						var total_basic_taxable = (cutoff[i].total_monthly_salary + cutoff[i].total_holiday_pay + cutoff[i].total_overtime_pay + cutoff[i].total_leave_pay + cutoff[i].total_nd_pay) - (cutoff[i].total_absences + cutoff[i].total_tardiness_pay + cutoff[i].total_sss_phic_hdmf);
						var total_taxable = (cutoff[i].total_monthly_salary + cutoff[i].total_holiday_pay + cutoff[i].total_overtime_pay + cutoff[i].total_leave_pay + cutoff[i].total_nd_pay) - (cutoff[i].total_absences + cutoff[i].total_tardiness_pay + cutoff[i].total_sss_phic_hdmf) + cutoff[i].pay13th_other_benefit_excess82000 + cutoff[i].sil_convert_excess;

						tmp.push({
							employee_id : cutoff[i].employee_id,
							tin : cutoff[i].tin,

							pay_13th_non_tax : cutoff[i].pay13th_other_benefit_below82000,
							total_sss_phic_hdmf_non_tax : cutoff[i].total_sss_phic_hdmf,
							salarie_other_non_tax : cutoff[i].sil_convert_below,
							total_non_tax : total_non_tax,

							total_basic_taxable : total_basic_taxable,
							pay_13th_non_tax : cutoff[i].pay13th_other_benefit_excess82000,
							salarie_other_taxable : cutoff[i].sil_convert_excess,
							total_taxable: total_taxable,

							total_gross_compensation : total_non_tax + total_taxable,
							tax_status : cutoff[i].tax_status,
							amount_of_exemption : 0 /*annual_tax_years*/,
							premium_paid_on_health : 0 /*optional*/,
							taxable_income : total_taxable,
							tax_due : 0 ,
							tax_withheld : 0 ,
							amount_withheld_and_paid_dec : 0,
							over_withheld_tax_refund : 0 ,
							amount_of_tax_withheld : 0 /*tax_due + amount_withheld_and paid_dec or tax_due - over_withheld_tax_refund*/,
						})

					}
					cb(false, tmp);
				});
			}
		],(err,r)=>{
			if(err){ r[0] = err; result_sql(r[0],res2);}
			else{
				result_sql2(r,res2);
			}

		});
	});




	app.post("/bir_alphalist/search",(req,res2)=>{

		
		async.waterfall([
			(cb)=>{
				var departments = parseInt(req.body.department);
				var year = parseInt(req.body.year);
				Call_procedure('bir_alphalist',{
					department : departments,
					year : year
				},(e,d)=>{
					//return res2.status(200).json([d]);
					if(e){ return cb(e, false); }

					connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{

						if(e){ return cb(e, false); }
						else{
							var tmp_employee = [];
							if(r[0].length > 0){
								try {
									var tmp_json = JSON.parse(r[0][0].account);
								} catch (e) {

									return res2.status(200).json(r);
								}

								async.eachSeries(tmp_json,(str, next)=>{
									async.setImmediate(()=>{
										str.detail.employee_id = str.employee_id;
										var sp = str.employee_name.split(" ");
										for(var z=0; z < sp.length; z++){
											if(z == 1){
												str.detail.first_name = sp[z];
											}
											else if(z == 2){
												str.detail.middle_name = sp[z];
											}
											else{
												if(str.detail.last_name){
													str.detail.last_name += sp[z];
												}else{
													str.detail.last_name = sp[z];
												}
											}
										}
										str.detail.employee_name = str.employee_name;

										connection.query("select z.tin from db_hrms_uat.emp z where z.employee_id = ?",[str.detail.employee_id], (e,r)=>{

											str.detail.tin = null;
											if(r.length){
												str.detail.tin = r[0].tin;
											}
											str.detail.local_it = str.local_it;
											tmp_employee.push(str.detail);
											next(null);
										});
									})
								},(e)=>{
									//return res2.status(200).json(tmp_employee);
									cb(false, tmp_employee);
								})
								// for(var i =0; i < tmp_json.length; i++){

								// 	tmp_json[i].detail.employee_id = tmp_json[i].employee_id;
								// 	var sp = tmp_json[i].employee_name.split(" ");
								// 	for(var z=0; z < sp.length; z++){
								// 		if(z == 1){
								// 			tmp_json[i].detail.first_name = sp[z];
								// 		}
								// 		else if(z == 2){
								// 			tmp_json[i].detail.middle_name = sp[z];
								// 		}
								// 		else{
								// 			if(tmp_json[i].detail.last_name){
								// 				tmp_json[i].detail.last_name += sp[z];
								// 			}else{
								// 				tmp_json[i].detail.last_name = sp[z];
								// 			}
								// 		}
								// 	}
								// 	tmp_json[i].detail.employee_name = tmp_json[i].employee_name;

								// 	connection.query("select z.tin from db_hrms_uat.emp z where z.employee_id = ?",[tmp_json[i].detail.employee_id], (e,r)=>{
								// 		tmp_json[i].detail.tin = null;
								// 		if(r){
								// 			tmp_json[i].detail.tin = r.tin;
								// 		}
								// 		tmp_json[i].detail.local_it = tmp_json[i].local_it;
								// 		tmp_employee.push(tmp_json[i].detail);
								// 	});
								// }
								//cb(false, tmp_employee);
							}else{ cb(msg_empty, false); }
						}
					})
				});
			},(cutoff, cb)=>{
				/*for(var i=0; i < cutoff.length; i++){
					cutoff[i].total_basic_ = (cutoff[i].total_monthly_salary + cutoff[i].total_holiday_pay + cutoff[i].total_overtime_pay + cutoff[i].total_leave_pay + cutoff[i].total_nd_pay) - (cutoff[i].total_absences + cutoff[i].total_tardiness_pay + cutoff[i].total_sss_phic_hdmf);
				}*/
				//return res2.status(200).json(cutoff);
				async.eachSeries(cutoff,(str,next)=>{
					async.setImmediate(function () {

						async.waterfall([
							(cb_1)=>{

								connection.query("select * from `cut_off_years` where `employee_id` = ? and `year` = ?",[str.employee_id,str.year],(e,r)=>{
									if(e){ return console.log(e,114);/*cb(e, false);*/ }
									else{

										var tmp_employee_id = [];
										return res2.status(200).json(r);
										if(r.length > 0){

											str.pay13th_other_benefit_below82000 = r[0][0].pay13th + r[0][0].performance_bonus + r[0][0].management_bonus + total_monthly_incentive;

											if(str.pay13th_other_benefit_below82000 > 82000){
												str.pay13th_other_benefit_excess82000 = str.pay13th_other_benefit_below82000 - 82000;
											}else{
												str.pay13th_other_benefit_excess82000 = 0;
											}

											//return cb1(false, r);
										}else{
											str.pay13th_other_benefit_below82000 	= 0;
											str.pay13th_other_benefit_excess82000 	= 0;
										}
										cb_1(false, str);
									}
								})

							},(str, cb_1)=>{

								

								connection.query("select * from `service_incentive_cutoff` where `employee_id` = ? and `year` = ?",[str.employee_id,str.year], (e,r)=>{
									if(e){ return console.log(11);/*cb(e, false);*/ }
									else{

										var tmp_employee_id = [];
										if(r.length > 0){

											var total_days_sil = r[0][0].vl + r[0][0].sl;
											if(total_days_sil > 10){
												str.sil_convert_excess = ((total_days_sil - 10) * r[0][0].daily_rate);
											}else{
												str.sil_convert_below = str.total_ecola + r[0][0].total_convertion;
											}
										}else{
											str.sil_convert_below 	= str.total_ecola;
											str.sil_convert_excess 	= 0;
										}
										cb_1(false, str);
									}
								})
							}
						],(err, str)=>{
							str = str;
							next(null);
						})
					});
				},(err)=>{
					var tmp = []
					for(var i=0; i < cutoff.length; i++){

						//return res2.status(200).json(cutoff);
						var total_non_tax = cutoff[i].pay13th_other_benefit_below82000 + cutoff[i].total_sss_phic_hdmf + cutoff[i].sil_convert_below;
						var total_basic_taxable = (cutoff[i].monthly_salary_rate.total + cutoff[i].total_holiday_pay.total + cutoff[i].total_overtime_pay.total + cutoff[i].total_leave_pay.total + cutoff[i].total_nd_pay.total) - (cutoff[i].total_absences.total + cutoff[i].total_tardiness_pay.total /*+ cutoff[i].total_sss_phic_hdmf.total*/);
						var total_taxable = (cutoff[i].monthly_salary_rate.total + cutoff[i].total_holiday_pay.total + cutoff[i].total_overtime_pay.total + cutoff[i].total_leave_pay.total + cutoff[i].total_nd_pay.total) - (cutoff[i].total_absences.total + cutoff[i].total_tardiness_pay.total /*+ cutoff[i].total_sss_phic_hdmf.total*/) + cutoff[i].pay13th_other_benefit_excess82000 + cutoff[i].sil_convert_excess;

						tmp.push({
							first_name : cutoff[i].first_name,
							middle_name : cutoff[i].middle_name,
							last_name : cutoff[i].last_name,
							employee_id : cutoff[i].employee_id,
							tin : cutoff[i].tin,

							pay_13th_non_tax : cutoff[i].pay13th_other_benefit_below82000,
							total_sss_phic_hdmf_non_tax : cutoff[i].total_sss_phic_hdmf,
							salarie_other_non_tax : cutoff[i].sil_convert_below,
							total_non_tax : total_non_tax,

							total_basic_taxable : total_basic_taxable,
							pay_13th_non_tax : cutoff[i].pay13th_other_benefit_excess82000,
							salarie_other_taxable : cutoff[i].sil_convert_excess,
							total_taxable: total_taxable,

							total_gross_compensation : total_non_tax + total_taxable,
							tax_status : cutoff[i].tax_status,
							amount_of_exemption : 0 /*annual_tax_years*/,
							premium_paid_on_health : 0 /*optional*/,
							taxable_income : total_taxable,
							tax_due : 0 ,
							tax_withheld : 0 ,
							amount_withheld_and_paid_dec : 0,
							over_withheld_tax_refund : 0 ,
							amount_of_tax_withheld : 0 /*tax_due + amount_withheld_and paid_dec or tax_due - over_withheld_tax_refund*/,
						})

					}
					cb(false, tmp);
				});
			}
		],(err,r)=>{
			if(err){
				msg_empty.header.message = e;
				return res2.status(msg_empty.header.status).json(msg_empty);
			}else{
				msg_success.data = r;
				return res2.status(msg_success.header.status).json(msg_success);
			}
			// if(err){ r[0] = err; result_sql(r[0],res2);}
			// else{
			// 	result_sql2(r,res2);
			// }

		});
	});

	/*});  END SQL CONNECTION*/

	app.listen(3014, () => {
		console.log("Server BIR ALPHALIST running on port 3014");
	});
